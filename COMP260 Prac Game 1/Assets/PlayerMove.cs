﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

	// Use this for initialization
	
    public Vector2 move;
    public Vector2 velocity;
    public float maxSpeed = 5.0f;    public float acceleration = 3.0f; // in metres/second/second
    private float speed = 0.0f; // in metres/second
    public float brake = 5.0f; // in metres/second/second
    public bool player1;
    public string Vertical;
    public string Horizontal;
    public float turnSpeed = 30.0f; // in degrees/second


    private BeeSpawner beeSpawner;
    void Start()
    {
        // find the bee spawner and store a reference for later
        beeSpawner = FindObjectOfType<BeeSpawner>();
    }    public float destroyRadius = 1.0f;
    // Update is called once per frame
    void Update () {
        if (Input.GetButtonDown("Fire1"))
        {
            // destroy nearby bees
            beeSpawner.DestroyBees(
            transform.position, destroyRadius);
        }

        /*
        // get the input values
        Vector2 direction;
        

        //if (player1 == true)
        //{
            direction.x = Input.GetAxis(Horizontal);
            direction.y = Input.GetAxis(Vertical);
            
        //}
        //else
        //{
            //direction.x = Input.GetAxis("Horizontal2");
            //direction.y = Input.GetAxis("Vertical2");
            // scale by the maxSpeed parameter
            
        //}
        // move the object
        Vector2 velocity = direction * maxSpeed;
        transform.Translate(velocity * Time.deltaTime);

        */

        // the horizontal axis controls the turn
        float turn = -Input.GetAxis("Horizontal");
        turnSpeed = 30.0f * speed;
        // turn the car
        transform.Rotate(0, 0, turn * turnSpeed * Time.deltaTime);

        float forwards = Input.GetAxis("Vertical");
        if (forwards > 0)
        {
            // accelerate forwards
            speed = speed + acceleration * Time.deltaTime;
        }
        else if (forwards < 0)
        {
            // accelerate backwards
            speed = speed - acceleration * Time.deltaTime;
        }
        else
        {
            // braking
            if (speed > 0)
            {
                speed = speed - brake * Time.deltaTime;
                if (speed < 0)
                {
                    speed = 0;
                }
            }
            else
            {
                speed = speed + brake * Time.deltaTime;
                if (speed > 0)
                {
                    speed = 0;
                }
            }
            /*if (speed < brake && speed > 0)
            {
                speed = 0;
            } else if(speed > -brake && speed < 0)
            {
                speed = 0;
            }*/
        }

        // clamp the speed
        speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);

        // compute a vector in the up direction of length speed
        Vector2 velocity = Vector2.up * speed;
        // move the object
        transform.Translate(velocity * Time.deltaTime, Space.Self);
    }


}

