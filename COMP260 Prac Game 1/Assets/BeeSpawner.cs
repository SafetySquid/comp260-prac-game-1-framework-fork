﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour {
    public BeeMove beePrefab;
    public int nBees = 50;
    public Rect spawnRect;
    // Use this for initialization
    void Start () {
        // create bees
        for (int i = 0; i < nBees; i++)
        {
            // instantiate a bee
            BeeMove bee = Instantiate(beePrefab);
            // attach to this object in the hierarchy
            bee.transform.parent = transform;
            // give the bee a name and number
            bee.gameObject.name = "Bee " + i;
            // move the bee to a random position within
            // the spawn rectangle
            float x = spawnRect.xMin +
            Random.value * spawnRect.width;
            float y = spawnRect.yMin +
            Random.value * spawnRect.height;
            bee.transform.position = new Vector2(x, y);
        }

    }


    // instantiate a bee



    
    void OnDrawGizmos()
    {
        // draw the spawning rectangle
        Gizmos.color = Color.green;
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMin, spawnRect.yMin),
         new Vector2(spawnRect.xMax, spawnRect.yMin));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMax, spawnRect.yMin),
         new Vector2(spawnRect.xMax, spawnRect.yMax));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMax, spawnRect.yMax),
         new Vector2(spawnRect.xMin, spawnRect.yMax));
        Gizmos.DrawLine(
         new Vector2(spawnRect.xMin, spawnRect.yMax),
         new Vector2(spawnRect.xMin, spawnRect.yMin));
    }

    // Update is called once per frame
    void Update () {
		
	}
    public void DestroyBees(Vector2 centre, float radius)
    {
        // destroy all bees within ‘radius’ of ‘centre’
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            // BUG! the line below doesn’t work -- FIXED --
            Vector2 v = (Vector2)child.position - centre;
            if (v.magnitude <= radius)
            {
                Destroy(child.gameObject);
            }
        }
    }
}

