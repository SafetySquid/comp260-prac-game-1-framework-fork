﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {

    //public float speed = 4.0f; // metres per second
    //public float turnSpeed = 180.0f; // degrees per second
    public Transform target;
    public Transform target2;
    private Vector2 heading = Vector2.right;

    // public parameters with default values
    public float minSpeed, maxSpeed;
    public float minTurnSpeed, maxTurnSpeed;

    // private state
    private float speed;
    private float turnSpeed;

    void Start()
    {
        PlayerMove player = (PlayerMove)
       FindObjectOfType(typeof(PlayerMove));
        target = player.transform;
        

        // bee initially moves in random direction
        heading = Vector2.right;
        float angle = Random.value * 360;
        heading = heading.Rotate(angle);
        // set speed and turnSpeed randomly
        speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
        turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed,
        Random.value);

        //PlayerMove player2 = (PlayerMove)
        //FindObjectOfType(typeof(PlayerMove));
        //target2 = player2.transform;
    }

    void Update()
    {
        // get the vector from the bee to the target
        Vector2 direction = target.position - transform.position;
        //Vector2 direction2 = target2.position - transform.position;
        
        //if(direction2.magnitude < direction.magnitude)
        //{
        //    direction = direction2;
        //}
        
        // calculate how much to turn per frame
        float angle = turnSpeed * Time.deltaTime;
        // turn left or right
        if (direction.IsOnLeft(heading))
        {
            // target on left, rotate anticlockwise
            heading = heading.Rotate(angle);
        }
        else
        {
            // target on right, rotate clockwise
            heading = heading.Rotate(-angle);
        }
        transform.Translate(heading * speed * Time.deltaTime);

    }
    void OnDrawGizmos()
    {
        // draw heading vector in red
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, heading);
        // draw target vector in yellow
        Gizmos.color = Color.yellow;
        Vector2 direction = target.position - transform.position;
        Gizmos.DrawRay(transform.position, direction);
    }

    public ParticleSystem explosionPrefab;
    void OnDestroy()
    {
        // create an explosion at the bee's current position
        ParticleSystem explosion = Instantiate(explosionPrefab);
        explosion.transform.position = transform.position;
    }
}
